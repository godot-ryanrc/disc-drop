extends StaticBody2D

#export var points = 100 setget _set_points
export var points = 100

var held_objects = {}
var level = null


func _ready():
	level = self.get_parent().get_parent()
	var lbl_points = self.get_node("./lbl_points:")
	lbl_points.text = str(points)
	lbl_points.update()

func _process(delta):
	for key in held_objects.keys():
		var elapsed = (Engine.get_frames_drawn() - held_objects[key]) / Engine.get_frames_per_second()
		if elapsed > 1.5:
			var cam = key.get_node("./camera:")
			if cam.current == true:
				var level_cam = self.get_parent().get_parent().get_node("./camera")
				level_cam.make_current()



func _on_Area2D_body_entered(body):
	if body.get("disc_id"):
		print("Disc(", body.disc_id, ") Entered: ", points)
		held_objects[body] = Engine.get_frames_drawn()
		body.in_bucket = true
		level.update_score(points)


func _on_Area2D_body_exited(body):
	if body.get("disc_id"):
		if body in held_objects:
			var frames_entered = held_objects[body]
			body.in_bucket = false
			var total_frames = Engine.get_frames_drawn() - frames_entered
			print("Frames in bucket: ", total_frames)
			print("Time in bucket:   ", total_frames * Engine.get_frames_per_second())
			level.update_score(points * -1)
