extends TextureButton

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var click_position = null

signal zone_create_clicked(position)

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _input(event):
	if event is InputEventMouseButton or event is InputEventScreenTouch:
		if event.is_pressed():
			pass
		else:
			click_position = event.position

func _on_zone_create_pressed():
	if click_position != null:
		emit_signal("zone_create_clicked", click_position)
		click_position = null

