extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var discs = 3
export var score = 0
var active_discs = []
var spawn_disc = true # Only allow one disk creation per frame
var is_reset = true

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	active_discs = self.get_node("discs")

func restart_level():
	# SceneTree.reload_current_scene()
	is_reset = true
	for bucket in self.get_node("buckets").get_children():
		bucket.held_objects = {}

	for disc in active_discs.get_children():
		disc.sleeping = true
		disc.queue_free()
	set_score(0)
	self.get_node("./camera:").make_current()


func set_score(value):
	score = value
	print("Current Score:  ", score)
	var lbl_points = self.get_node("./lbl_score:")
	lbl_points.text = "Score: " + str(score)
	lbl_points.update()

func update_score(value):
	if not is_reset:
		print("Score Update: ", value)
		set_score(score + value)

func create_disk(location):
	if spawn_disc and len(active_discs.get_children()) < discs:
		is_reset = false
		var node = self.get_node("./disc:").duplicate()
		active_discs.add_child(node)
		node.linear_velocity = Vector2(0, 0)
		node.position = location
		node.disc_id = len(active_discs.get_children())
		node.visible = true
		var btn_track = self.get_node("./btn_track")
		if btn_track.pressed:
			var cam = node.get_node("./camera:")
			cam.make_current()

		spawn_disc = false

func _process(delta):
	spawn_disc = true

func _on_zone_create_zone_create_clicked(position):
	create_disk(position)


func _on_btn_restart_pressed():
	restart_level()


func _on_btn_track_toggled(button_pressed):
	if button_pressed:
		var last_disc = null
		for disc in active_discs.get_children():
			last_disc = disc
		if last_disc != null:
			last_disc.get_node("./camera:").make_current()
	else:
		if self.get_node("./camera:").current == false:
			self.get_node("./camera:").make_current()
